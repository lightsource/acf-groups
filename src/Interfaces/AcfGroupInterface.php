<?php

declare(strict_types=1);

namespace LightSource\AcfGroups\Interfaces;

use Exception;

interface AcfGroupInterface extends GroupInfoInterface
{
    /**
     * @param array<int|string,mixed> $rows
     * @return array<int|string,mixed>
     */
    public static function convertRepeaterFieldValues(
        string $repeaterFieldName,
        array $rows,
        bool $isFromAcfFormat = true
    ): array;

    /**
     * @param array<string,mixed> $fields
     * @return array<int|string,mixed>
     */
    public static function convertCloneField(
        string $cloneFieldName,
        array $fields,
        bool $isFromAcfFormat = true
    ): array;

    /**
     * @param false|string|int $source
     * @param array<string,mixed>|null $externalData Can be output of the 'getFieldValues()' method
     * @throws Exception
     */
    public function load($source = false, string $clonePrefix = '', ?array $externalData = null): bool;

    public function loadFromPostContent(int $postId): bool;

    public function isExternalSource(): bool;

    public function isLoaded(): bool;

    public function isHasChanges(): bool;

    /**
     * @return string[]
     */
    public function getRepeaterFieldNames(): array;

    /**
     * @return string[]
     */
    public function getCloneFieldNames(): array;

    public function refreshFieldValuesCache(): void;

    public function save(bool $isForce = false): bool;

    public function getJson(bool $isSkipDefaults = false): string;

    /**
     * @param array<string,mixed> $postFields Can be used to update other post fields (in the same query)
     *
     * @return bool
     */
    public function saveToPostContent(array $postFields = []): bool;

    /**
     * @return array<string,mixed>
     */
    public function getFieldValues(string $clonePrefix = '', bool $isSkipDefaults = false): array;

    /**
     * @return int|string|false
     */
    public function getSource();

    /**
     * @return array<string,mixed>|null
     */
    public function getExternalData(): ?array;

    public function getClonePrefix(): string;

    public function setClonePrefix(string $clonePrefix): void;

    public function getDeepClone(): AcfGroupInterface;
}
