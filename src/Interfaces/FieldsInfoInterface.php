<?php

declare(strict_types=1);

namespace LightSource\AcfGroups\Interfaces;

use LightSource\AcfGroups\FieldInfo;
use Exception;

interface FieldsInfoInterface
{
    /**
     * @return FieldInfoInterface[]
     * @throws Exception
     */
    public static function getFieldsInfo(): array;
}
