<?php

declare(strict_types=1);

use Codeception\Test\Unit;
use LightSource\AcfGroups\GroupInfo;
use LightSource\AcfGroups\Interfaces\FieldInfoInterface;
use LightSource\AcfGroups\Interfaces\GroupInfoInterface;
use LightSource\AcfGroups\Interfaces\CreatorInterface;


class GroupInfoTest extends Unit
{
    private function getCreator(): CreatorInterface
    {
        $creator = null;

        $creator = $this->makeEmpty(CreatorInterface::class, [
            'create' => function (string $class) use (&$creator) {
                return new $class($creator);
            },
        ]);

        return $creator;
    }

    private function getFieldInfo(
        string $name = '',
        string $type = '',
        array $arguments = []
    ): FieldInfoInterface {
        return $this->makeEmpty(FieldInfoInterface::class, [
            'getType' => function () use ($type) {
                return $type;
            },
            'getName' => function () use ($name) {
                return $name;
            },
            'getArguments' => function () use ($arguments) {
                return $arguments;
            },
            'isRepeater' => function () use ($type, $arguments) {
                return 'array' === $type &&
                    isset($arguments['item']);
            },
        ]);
    }

    private function getGroup(
        CreatorInterface $creator,
        string $method,
        array $fieldsInfo,
        array $locationRules = [],
        string $class = 'Link',
        string $classCode = ''
    ): GroupInfoInterface {
        $GLOBALS[$method . 'fi'] = $fieldsInfo;
        $GLOBALS[$method . 'lr'] = $locationRules;

        $namespace = str_replace('::', '_', $method);
        eval(
            'namespace ' . $namespace . ';  class ' . $class . ' extends \LightSource\AcfGroups\GroupInfo implements \LightSource\AcfGroups\Interfaces\AcfGroupInterface{
 public static function convertRepeaterFieldValues(
        string $repeaterFieldName,
        array $rows,
        bool $isFromAcfFormat = true
    ): array{return [];}
    public static function convertCloneField(
        string $cloneFieldName,
        array $fields,
        bool $isFromAcfFormat = true
    ): array{return [];}
 public function load($source = false, string $clonePrefix = "",?array $externalData = null): bool{return false;}
  public function loadFromPostContent(int $postId): bool{return false;}
   public function isExternalSource(): bool{return false;}
    public function isLoaded(): bool{return false;}
    public function isHasChanges(): bool{return false;}
    public function save(bool $isForce = false): bool{return false;}
     public function saveToPostContent(array $postFields=[]): bool{return false;}
    public function getFieldValues(string $clonePrefix = "", bool $isSkipDefaults = false):array{return [];}
    public function getSource(){}
     public function getExternalData(): ?array{return null;}
    public function refreshFieldValuesCache(): void{}
    public function setSource($source):void{}
    public function getClonePrefix():string{return "";}
    public function setClonePrefix(string $clonePrefix):void{}
     public function getRepeaterFieldNames(): array{return [];}
    public function getCloneFieldNames(): array{return [];}
    protected static function getLocationRules():array{return $GLOBALS["' . $method . 'lr"]; }
    public function getDeepClone():\LightSource\AcfGroups\Interfaces\AcfGroupInterface{return clone $this;}
    public function getJson(bool $isSkipDefaults = false): string{return "";}
    public static function getFieldsInfo():array{
return $GLOBALS["' . $method . 'fi"];
}' . $classCode . '
        };'
        );
        $fullClass = $namespace . '\\' . $class;
        $group = $creator->create($fullClass);

        return $group;
    }

    private function getGroupInfo(
        CreatorInterface $creator,
        string $method,
        array $fieldsInfo,
        array $locationRules = []
    ): array {
        $group = $this->getGroup($creator, $method, $fieldsInfo, $locationRules);

        return $group->getGroupInfo();
    }

    public function testGetAcfGroupName()
    {
        $namespace = str_replace('::', '_', __METHOD__);
        eval('namespace ' . $namespace . ';  class Link extends \LightSource\AcfGroups\GroupInfo{};$group=new Link(new \LightSource\AcfGroups\Creator());');
        $group = $group ?? null;

        $this->assertEquals('group_local_link', $group::getAcfGroupName());
    }

    public function testGetAcfGroupNameWhenCustomGroupNameIsSet()
    {
        $namespace = str_replace('::', '_', __METHOD__);
        eval(
            'namespace ' . $namespace . '; class Group extends \LightSource\AcfGroups\GroupInfo{
protected static function getCustomGroupName() : string{return "custom-name";}};
$group=new Group(new \LightSource\AcfGroups\Creator());'
        );
        $group = $group ?? null;

        $this->assertEquals('custom-name', $group::getAcfGroupName());
    }

    public function testGetAcfGroupNameConvertsCamelCaseWithDashes()
    {
        $namespace = str_replace('::', '_', __METHOD__);
        eval(
            'namespace ' . $namespace . '; class SomeGroup extends \LightSource\AcfGroups\GroupInfo{};
$group=new SomeGroup(new \LightSource\AcfGroups\Creator());'
        );
        $group = $group ?? null;

        $this->assertEquals('group_local_some-group', $group::getAcfGroupName());
    }

    public function testGetAcfFieldName()
    {
        $group = new class extends GroupInfo {
            protected static function getGroupName(): string
            {
                return 'Link';
            }
        };

        $this->assertEquals('group_local_link__field', $group->getAcfFieldName('field'));
    }

    public function testGetAcfFieldNameWhenFieldNamePrefixIsChanged()
    {
        $group = new class extends GroupInfo {
            const FIELD_NAME_PREFIX = '';

            protected static function getGroupName(): string
            {
                return 'Link';
            }
        };

        $this->assertEquals('group_local_link__field', $group->getAcfFieldName('field'));
    }

    public function testGetAcfFieldNameWhenGroupNamePrefixIsChanged()
    {
        $group = new class extends GroupInfo {
            const GROUP_NAME_PREFIX = '';

            protected static function getGroupName(): string
            {
                return 'Link';
            }
        };

        $this->assertEquals('link__url', $group->getAcfFieldName('url'));
    }

    public function testGetAcfFieldNameWhenGroupAndFieldNamePrefixIsChanged()
    {
        $group = new class extends GroupInfo {
            const GROUP_NAME_PREFIX = '';
            const FIELD_NAME_PREFIX = '';

            protected static function getGroupName(): string
            {
                return 'Link';
            }
        };

        $this->assertEquals('link__url', $group->getAcfFieldName('url'));
    }

    public function testGetAcfFieldNameWhenCustomGroupNameIsSet()
    {
        $group = new class extends GroupInfo {
            const CUSTOM_GROUP_NAME = 'link';
        };

        $this->assertEquals('link__url', $group->getAcfFieldName('url'));
    }

    public function testGetAcfFieldNameWhenCustomGroupNameAndFieldNamePrefixIsSet()
    {
        $group = new class extends GroupInfo {
            const CUSTOM_GROUP_NAME = 'link';
            const FIELD_NAME_PREFIX = '';
        };

        $this->assertEquals('link__url', $group->getAcfFieldName('url'));
    }

    public function testGetAcfFieldNameCovertsCamelCaseToDashed()
    {
        $group = new class extends GroupInfo {
            public static function getAcfGroupName(): string
            {
                return 'group_link';
            }
        };

        $this->assertEquals('group_link__field-name', $group->getAcfFieldName('fieldName'));
    }

    public function testGetGroupInfoKey(): void
    {
        $groupInfo = $this->getGroupInfo($this->getCreator(), __METHOD__, [
            $this->getFieldInfo('fieldName', 'string', ['required' => 1,]),
        ]);

        $this->assertEquals('group_local_link', $groupInfo['key'],);
    }

    public function testGetGroupInfoTitle(): void
    {
        $groupInfo = $this->getGroupInfo($this->getCreator(), __METHOD__, [
            $this->getFieldInfo('fieldName', 'string', ['required' => 1,]),
        ]);

        $this->assertEquals('Link', $groupInfo['title'],);
    }

    public function testGetGroupInfoLocation(): void
    {
        $groupInfo = $this->getGroupInfo($this->getCreator(), __METHOD__, [], [
            [
                'post_type == page',
            ],
        ]);

        $this->assertEquals(
            [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'page',
                    ],
                ],
            ],
            $groupInfo['location'],
        );
    }

    public function testGetGroupInfoLocationWhenTypeIsAND(): void
    {
        $groupInfo = $this->getGroupInfo($this->getCreator(), __METHOD__, [], [
            [
                'post_type == page',
                'page_template == template',
            ],
        ]);

        $this->assertEquals(
            [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'page',
                    ],
                    [
                        'param' => 'page_template',
                        'operator' => '==',
                        'value' => 'template',
                    ],
                ],
            ],
            $groupInfo['location'],
        );
    }

    public function testGetGroupInfoLocationWhenTypeIsOR(): void
    {
        $groupInfo = $this->getGroupInfo($this->getCreator(), __METHOD__, [], [
            [
                'post_type == page',
            ],
            [
                'page_template == template',
            ],
        ]);

        $this->assertEquals(
            [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'page',
                    ],
                ],
                [
                    [
                        'param' => 'page_template',
                        'operator' => '==',
                        'value' => 'template',
                    ],
                ],
            ],
            $groupInfo['location'],
        );
    }

    public function testGetGroupInfoConvertsATypeFieldArgumentToType(): void
    {
        $groupInfo = $this->getGroupInfo($this->getCreator(), __METHOD__, [
            $this->getFieldInfo('fieldName', 'string', ['a-type' => 'textarea',]),
        ]);

        $this->assertArrayNotHasKey('a-type', $groupInfo['fields'][0]);
        $this->assertEquals('textarea', $groupInfo['fields'][0]['type']);
    }

    public function testGetGroupInfoField(): void
    {
        $groupInfo = $this->getGroupInfo($this->getCreator(), __METHOD__, [
            $this->getFieldInfo('fieldName', 'string', ['required' => 1,]),
        ]);

        $this->assertEquals(
            [
                [
                    'key' => 'group_local_link__field-name',
                    'label' => 'Field Name',
                    'name' => 'group_local_link__field-name',
                    'type' => 'text',
                    'required' => 1,
                ],
            ],
            $groupInfo['fields'],
        );
    }

    public function testGetGroupInfoCloneField(): void
    {
        $creator = $this->getCreator();
        $subGroup = $this->getGroup($creator, __METHOD__, [], [], 'SubGroup');
        $mainGroup = $this->getGroup($creator, __METHOD__, [
            $this->getFieldInfo('subGroup', get_class($subGroup)),
        ]);

        $this->assertEquals(
            [
                [
                    'key' => 'group_local_link__sub-group__tab',
                    'label' => 'Sub Group',
                    'name' => '',
                    'type' => 'tab',
                    'open' => 0,
                    'multi_expand' => 0,
                    'endpoint' => 0,
                ],
                [
                    'key' => 'group_local_link__sub-group',
                    'label' => 'Sub Group',
                    'name' => 'group_local_link__sub-group',
                    'type' => 'clone',
                    'clone' => [
                        'group_local_sub-group',
                    ],
                    'display' => 'group',
                    'layout' => 'block',
                    'prefix_label' => 0,
                    'prefix_name' => 1,
                ],
            ],
            $mainGroup->getGroupInfo()['fields'],
        );
    }

    public function testGetGroupInfoCloneFieldWhenNoTabArgumentIsSet(): void
    {
        $subGroup = $this->getGroup($this->getCreator(), __METHOD__, [], [], 'SubGroup');
        $mainGroup = $this->getGroup($this->getCreator(), __METHOD__, [
            $this->getFieldInfo('subGroup', get_class($subGroup), ['a-no-tab' => 1,]),
        ]);

        $this->assertEquals(
            [
                [
                    'key' => 'group_local_link__sub-group',
                    'label' => 'Sub Group',
                    'name' => 'group_local_link__sub-group',
                    'type' => 'clone',
                    'clone' => [
                        'group_local_sub-group',
                    ],
                    'display' => 'group',
                    'layout' => 'block',
                    'prefix_label' => 0,
                    'prefix_name' => 1,
                ],
            ],
            $mainGroup->getGroupInfo()['fields'],
        );
    }

    public function testGetGroupInfoRepeaterField(): void
    {
        $subGroup = $this->getGroup($this->getCreator(), __METHOD__, [], [], 'SubGroup');
        $mainGroup = $this->getGroup($this->getCreator(), __METHOD__, [
            $this->getFieldInfo('subGroup', 'array', ['item' => get_class($subGroup),]),
        ]);

        $this->assertEquals(
            [
                [
                    'key' => 'group_local_link__sub-group__tab',
                    'label' => 'Sub Group',
                    'name' => '',
                    'type' => 'tab',
                    'open' => 0,
                    'multi_expand' => 0,
                    'endpoint' => 0,
                ],
                [
                    'key' => 'group_local_link__sub-group',
                    'label' => 'Sub Group',
                    'name' => 'group_local_link__sub-group',
                    'type' => 'repeater',
                    'layout' => 'row',
                    'sub_fields' => [
                        [
                            'key' => 'group_local_link__sub-group_item',
                            'label' => 'Sub Group',
                            'name' => 'group_local_link__sub-group_item',
                            'type' => 'clone',
                            'clone' => [
                                'group_local_sub-group',
                            ],
                            'display' => 'seamless',
                            'layout' => 'row',
                            'prefix_label' => 0,
                            'prefix_name' => 0,
                        ],
                    ],
                ],
            ],
            $mainGroup->getGroupInfo()['fields'],
        );
    }

    public function testGetGroupInfoRepeaterFieldWhenNoTabArgumentIsSet(): void
    {
        $subGroup = $this->getGroup($this->getCreator(), __METHOD__, [], [], 'SubGroup');
        $mainGroup = $this->getGroup($this->getCreator(), __METHOD__, [
            $this->getFieldInfo('subGroup', 'array', [
                'item' => get_class($subGroup),
                'a-no-tab' => 1,
            ]),
        ]);

        $this->assertEquals(
            [
                [
                    'key' => 'group_local_link__sub-group',
                    'label' => 'Sub Group',
                    'name' => 'group_local_link__sub-group',
                    'type' => 'repeater',
                    'layout' => 'row',
                    'sub_fields' => [
                        [
                            'key' => 'group_local_link__sub-group_item',
                            'label' => 'Sub Group',
                            'name' => 'group_local_link__sub-group_item',
                            'type' => 'clone',
                            'clone' => [
                                'group_local_sub-group',
                            ],
                            'display' => 'seamless',
                            'layout' => 'row',
                            'prefix_label' => 0,
                            'prefix_name' => 0,
                        ],
                    ],
                ],
            ],
            $mainGroup->getGroupInfo()['fields'],
        );
    }

    public function testGetGroupInfoTitleWhenMultilingual()
    {
        $group = new class extends GroupInfo {
            protected static function getMultilingualLabel(string $label): string
            {
                return 'Модель';
            }
        };

        $this->assertEquals('Модель', $group::getGroupInfo()['title']);
    }

    public function testGetGroupInfoFieldLabelWhenMultilingual()
    {
        $code = 'static function getMultilingualLabel(string $label):string{return "Beta"===$label?"Модель":$label;}';

        $group = $this->getGroup($this->getCreator(), __METHOD__, [
            $this->getFieldInfo('fieldName', 'string', ['required' => 1, 'label' => 'Beta',]),
        ], [], 'Link', $code);

        $this->assertEquals('Модель', $group::getGroupInfo()['fields'][0]['label']);
    }

    public function testGetGroupInfoFieldInstructionsWhenMultilingual()
    {
        $code = 'static function getMultilingualLabel(string $label):string{return "About using"===$label?"Про использование":$label;}';

        $group = $this->getGroup($this->getCreator(), __METHOD__, [
            $this->getFieldInfo('fieldName', 'string', ['required' => 1, 'instructions' => 'About using',]),
        ], [], 'Link', $code);

        $this->assertEquals('Про использование', $group::getGroupInfo()['fields'][0]['instructions']);
    }

    public function testGetGroupInfoFieldButtonLabelWhenMultilingual()
    {
        $code = 'static function getMultilingualLabel(string $label):string{return "Add new"===$label?"Добавить новый":$label;}';

        $group = $this->getGroup($this->getCreator(), __METHOD__, [
            $this->getFieldInfo('fieldName', 'string', ['required' => 1, 'button_label' => 'Add new',]),
        ], [], 'Link', $code);

        $this->assertEquals('Добавить новый', $group::getGroupInfo()['fields'][0]['button_label']);
    }

    public function testGetGroupInfoFieldChoicesWhenMultilingual()
    {
        $code = 'static function getMultilingualLabel(string $label):string{if("Default"===$label){return "Простой";}if("Masonry"===$label){return "Масонри";}return $label;}';

        $group = $this->getGroup($this->getCreator(), __METHOD__, [
            $this->getFieldInfo('fieldName', 'string', [
                'required' => 1,
                // direct array, not json. As it's a mock, and JSON decoding isn't available
                'choices' => [
                    'plain' => 'Default',
                    'masonry' => 'Masonry',
                ],
            ]),
        ], [], 'Link', $code);

        $this->assertEquals([
            'plain' => 'Простой',
            'masonry' => 'Масонри',
        ], $group::getGroupInfo()['fields'][0]['choices']);
    }
}
