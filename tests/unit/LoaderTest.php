<?php

declare(strict_types=1);

use Codeception\Test\Unit;
use LightSource\AcfGroups\AcfGroup;
use LightSource\AcfGroups\Interfaces\CreatorInterface;
use LightSource\AcfGroups\Loader;
use org\bovigo\vfs\vfsStream;

class GroupsLoaderTest extends Unit
{
    protected UnitTester $tester;

    private function getCreator(): CreatorInterface
    {
        $creator = null;

        $creator = $this->makeEmpty(CreatorInterface::class, [
            'create' => function (string $class) use (&$creator) {
                return new $class($creator);
            },
        ]);

        return $creator;
    }

    public function testSignUpGroups(): void
    {
        $rootDirectory = $this->tester->getUniqueDirectory(__METHOD__);
        $namespace = $this->tester->getUniqueNamespaceWithAutoloader(__METHOD__, $rootDirectory->url());
        $blocksFolder = vfsStream::create(
            [
                'FirstGroup.php' => $this->tester->getGroupClassFile(
                    $namespace,
                    'FirstGroup',
                    '\\' . AcfGroup::class
                ),
                'SecondGroup.php' => $this->tester->getGroupClassFile(
                    $namespace,
                    'SecondGroup',
                    '\\' . AcfGroup::class
                ),
            ],
            $rootDirectory
        );

        $loader = new Loader();

        $loader->signUpGroups($namespace, $blocksFolder->url());

        $this->assertEquals([
            $namespace . '\\FirstGroup',
            $namespace . '\\SecondGroup',
        ], $loader->getLoadedGroups());
    }

    public function testSignUpGroupsWhenCustomPregIsSet(): void
    {
        $rootDirectory = $this->tester->getUniqueDirectory(__METHOD__);
        $namespace = $this->tester->getUniqueNamespaceWithAutoloader(__METHOD__, $rootDirectory->url());
        $blocksFolder = vfsStream::create(
            [
                'SomeData.php' => $this->tester->getGroupClassFile(
                    $namespace,
                    'SomeData',
                    '\\' . AcfGroup::class
                ),
                'Extra.php' => $this->tester->getGroupClassFile(
                    $namespace,
                    'Extra',
                    '\\' . AcfGroup::class
                ),
            ],
            $rootDirectory
        );

        $loader = new Loader();

        $loader->signUpGroups($namespace, $blocksFolder->url(), '/Data.php$/');

        $this->assertEquals([
            $namespace . '\\SomeData',
        ], $loader->getLoadedGroups());
    }

    public function testSignUpGroupsWhenAbstractClassIsIgnored(): void
    {
        $rootDirectory = $this->tester->getUniqueDirectory(__METHOD__);
        $namespace = $this->tester->getUniqueNamespaceWithAutoloader(__METHOD__, $rootDirectory->url());
        $blocksFolder = vfsStream::create(
            [
                'FirstGroup.php' => $this->tester->getGroupClassFile(
                    $namespace,
                    'FirstGroup',
                    '\\' . AcfGroup::class,
                    '',
                    true
                ),
            ],
            $rootDirectory
        );

        $loader = new Loader($this->getCreator());

        $loader->signUpGroups($namespace, $blocksFolder->url());

        $this->assertEmpty($loader->getLoadedGroups());
    }

    public function testSignUpGroupsWhenNotLocalClassIsIgnored(): void
    {
        $rootDirectory = $this->tester->getUniqueDirectory(__METHOD__);
        $namespace = $this->tester->getUniqueNamespaceWithAutoloader(__METHOD__, $rootDirectory->url());
        $blocksFolder = vfsStream::create(
            [
                'FirstGroup.php' => $this->tester->getGroupClassFile(
                    $namespace,
                    'FirstGroup',
                    '\\' . AcfGroup::class,
                    'const IS_LOCAL_GROUP = false;'
                ),
            ],
            $rootDirectory
        );

        $loader = new Loader($this->getCreator());

        $loader->signUpGroups($namespace, $blocksFolder->url());

        $this->assertEmpty($loader->getLoadedGroups());
    }
}
