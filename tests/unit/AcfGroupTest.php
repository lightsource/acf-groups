<?php

declare(strict_types=1);

use Codeception\Test\Unit;
use LightSource\AcfGroups\AcfGroup;
use LightSource\AcfGroups\Creator;
use LightSource\AcfGroups\Interfaces\CreatorInterface;
use LightSource\AcfGroups\Interfaces\FieldInfoInterface;

class AcfGroupTest extends Unit
{
    private function getCreator(): CreatorInterface
    {
        $creator = null;

        $creator = $this->makeEmpty(CreatorInterface::class, [
            'create' => function (string $class) use (&$creator) {
                return new $class($creator);
            },
        ]);

        return $creator;
    }

    private function getFieldInfo(
        string $name = '',
        string $type = '',
        array  $arguments = []
    ): FieldInfoInterface
    {
        return $this->makeEmpty(FieldInfoInterface::class, [
            'getType' => function () use ($type) {
                return $type;
            },
            'getName' => function () use ($name) {
                return $name;
            },
            'getArguments' => function () use ($arguments) {
                return $arguments;
            },
            'isRepeater' => function () use ($type, $arguments) {
                return 'array' === $type &&
                    isset($arguments['item']);
            },
        ]);
    }

    public function testConstructorSetDefaultsForBool(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {
            public bool $fieldName;
        };

        $this->assertTrue(false === $group->fieldName);
    }

    public function testConstructorSetDefaultsForInt(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {
            public int $fieldName;
        };

        $this->assertTrue(0 === $group->fieldName);
    }

    public function testConstructorSetDefaultsForFloat(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {
            public float $fieldName;
        };

        $this->assertTrue(0.0 === $group->fieldName);
    }

    public function testConstructorSetDefaultsForString(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {
            public string $fieldName;
        };

        $this->assertTrue('' === $group->fieldName);
    }

    public function testConstructorSetDefaultsForClone(): void
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            public string $fieldName;
        };
        $fieldInfo = $this->getFieldInfo('sibling', get_class($siblingClass));
        $groupClass = new class ($fieldInfo, $creator) extends AcfGroup {
            public $sibling;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }
        };

        $this->assertEquals('', $groupClass->sibling->fieldName);
    }

    public function testConstructorSetDefaultsForRepeater(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {
            public array $fieldName;
        };

        $this->assertTrue([] === $group->fieldName);
    }

    ////

    public function testLoadField(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {

            public float $fieldName;

            public static function getAcfFieldName(string $fieldName): string
            {
                if ('fieldName' === $fieldName) {
                    return 'some_fieldName';
                }

                return '';
            }

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                $acfFieldName = $this->getAcfFieldNameWithClonePrefix($fieldInfo->getName());

                if ('some_fieldName' === $acfFieldName) {
                    return 2.00;
                }

                return null;
            }
        };

        $group->load();

        $this->assertTrue(2.00 === $group->fieldName);
    }

    public function testLoadBoolFieldWhenValueIsNull(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {

            public bool $fieldName;

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return null;
            }
        };

        $group->load();

        $this->assertTrue(false === $group->fieldName);
    }

    public function testLoadBoolFieldWhenValueWithAnotherType(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {

            public bool $fieldName;

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return 'test';
            }
        };

        $group->load();

        $this->assertTrue(true === $group->fieldName);
    }

    public function testLoadStringFieldWhenValueIsNull(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {

            public string $fieldName;

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return null;
            }
        };

        $group->load();

        $this->assertTrue('' === $group->fieldName);
    }

    public function testLoadStringFieldWhenValueWithAnotherType(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {

            public string $fieldName;

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return 1;
            }
        };

        $group->load();

        $this->assertTrue('1' === $group->fieldName);
    }

    public function testLoadIntFieldWhenValueIsNull(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {

            public int $fieldName;

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return null;
            }
        };

        $group->load();

        $this->assertTrue(0 === $group->fieldName);
    }

    public function testLoadIntFieldWhenValueWithAnotherType(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {

            public int $fieldName;

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return 'test';
            }
        };

        $group->load();

        $this->assertTrue(0 === $group->fieldName);
    }

    public function testLoadFloatFieldWhenValueIsNull(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {

            public float $fieldName;

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return null;
            }
        };

        $group->load();

        $this->assertTrue(0.0 === $group->fieldName);
    }

    public function testLoadFloatFieldWhenValueWithAnotherType(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {

            public float $fieldName;

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return 'test';
            }
        };

        $group->load();

        $this->assertTrue(0.0 === $group->fieldName);
    }

    public function testLoadCloneField(): void
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {

            public string $fieldName;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'fieldName';
            }

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                $acfFieldName = $this->getAcfFieldNameWithClonePrefix($fieldInfo->getName());
                if ('group_sibling_fieldName' === $acfFieldName) {
                    return 'fieldValue';
                }

                return '';
            }

        };
        $fieldInfo = $this->getFieldInfo('sibling', get_class($siblingClass));
        $groupClass = new class ($fieldInfo, $creator) extends AcfGroup {
            public $sibling;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_sibling';
            }
        };

        $groupClass->load();

        $this->assertEquals('group_sibling_', $groupClass->sibling->getClonePrefix());
        $this->assertEquals('fieldValue', $groupClass->sibling->fieldName);
    }

    public function testLoadCloneFieldThrowsExceptionWhenTypeIsWrong(): void
    {
        // using a real instance, because the class rely on that exception will be thrown by the creator
        $creator = new Creator();
        $fieldInfo = $this->getFieldInfo('sibling', get_class($creator));
        $groupClass = new class ($fieldInfo, $creator) extends AcfGroup {
            public $sibling;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            protected function setDefaultValuesForFields(): void
            {
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_sibling';
            }
        };

        $isWithException = false;

        try {
            $groupClass->load();
        } catch (Exception $exception) {
            $isWithException = true;
        }

        $this->assertTrue($isWithException);
    }

    public function testLoadRepeaterField(): void
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {

            public string $fieldName;

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return 'fieldValue';
            }

        };
        $fieldInfo = $this->getFieldInfo('repeater', 'array', [
            'item' => get_class($siblingClass),
        ]);
        $groupClass = new class ($fieldInfo, $creator) extends AcfGroup {
            public $repeater;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return [[], []];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_sibling';
            }
        };

        $groupClass->load();

        $this->assertEquals('group_sibling_0_', $groupClass->repeater[0]->getClonePrefix());
        $this->assertEquals('group_sibling_1_', $groupClass->repeater[1]->getClonePrefix());
        $this->assertEquals('fieldValue', $groupClass->repeater[0]->fieldName);
        $this->assertEquals('fieldValue', $groupClass->repeater[1]->fieldName);
    }

    public function testLoadRepeaterFieldThrowsExceptionWhenPhpDocItemArgumentIsWrong(): void
    {
        // using a real instance, because the class rely on that exception will be thrown by the creator
        $creator = new Creator();
        $fieldInfo = $this->getFieldInfo('repeater', 'array', [
            'item' => get_class($creator),
        ]);
        $groupClass = new class ($fieldInfo, $creator) extends AcfGroup {
            public $repeater;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return [[], []];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_sibling';
            }
        };

        $isWithException = false;

        try {
            $groupClass->load();
        } catch (Exception $exception) {
            $isWithException = true;
        }


        $this->assertTrue($isWithException);
    }

    //

    public function testLoadFromExternalData()
    {
        $creator = $this->getCreator();
        $group = new class ($creator) extends AcfGroup {
            public string $content;
            public int $price;

            public static function getAcfFieldName(string $fieldName): string
            {
                return $fieldName;
            }
        };
        $group->getDeepClone();

        $group->load(false, '', [
            'content' => 'some content',
            'price' => 2,
        ]);

        $this->assertEquals(2, $group->price);
        $this->assertEquals('some content', $group->content);
    }

    public function testLoadFromExternalDataWhenClone()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('sibling', get_class($siblingClass));
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $sibling;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_sibling';
            }
        };


        $group->load(false, '', [
            'group_sibling_sibling_content' => 'some content',
        ]);


        $this->assertEquals('some content', $group->sibling->content);
    }

    public function testLoadFromExternalDataWhenRepeater()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('repeater', 'array', [
            'item' => get_class($siblingClass),
        ]);
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $repeater;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_repeater';
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }
        };


        $group->load(false, '', [
            'group_repeater' => [
                [
                    'sibling_content' => 'some content',
                ],
            ],
        ]);


        $this->assertCount(1, $group->repeater);
        $this->assertEquals('some content', $group->repeater[0]->content);
    }

    ////

    public function testSaveField(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {
            public float $fieldName;
            private string $calledFieldName;
            private $calledFieldValue;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_fieldName';
            }

            protected function setAcfFieldValue(string $acfFieldName, $value): void
            {
                $this->calledFieldName = $acfFieldName;
                $this->calledFieldValue = $value;
                parent::setAcfFieldValue($acfFieldName, $value);
            }

            public function getCalledFieldName()
            {
                return $this->calledFieldName;
            }

            public function getCalledFieldValue()
            {
                return $this->calledFieldValue;
            }
        };

        $group->load();
        $group->fieldName = 2.0;
        $group->save();

        $this->assertEquals('group_fieldName', $group->getCalledFieldName());
        $this->assertTrue(2.0 === $group->getCalledFieldValue());
    }

    public function testSaveFieldIgnoresValueThatIsNotChanged(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {
            public float $fieldName;
            private string $calledFieldName;

            public function __construct(CreatorInterface $creator)
            {
                parent::__construct($creator);

                $this->calledFieldName = '';
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_fieldName';
            }

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return 2.0;
            }

            protected function setAcfFieldValue(string $acfFieldName, $value): void
            {
                $this->calledFieldName = $acfFieldName;
                parent::setAcfFieldValue($acfFieldName, $value);
            }

            public function getCalledFieldName()
            {
                return $this->calledFieldName;
            }
        };

        $group->load();
        $group->fieldName = 2.0;
        $group->save();

        $this->assertEmpty($group->getCalledFieldName());
    }

    public function testSaveFieldRefreshesTheOriginalFieldValueCache(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {
            public float $fieldName;
            private string $calledFieldName;

            public function __construct(CreatorInterface $creator)
            {
                parent::__construct($creator);

                $this->calledFieldName = '';
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_fieldName';
            }

            protected function setAcfFieldValue(string $acfFieldName, $value): void
            {
                $this->calledFieldName = $acfFieldName;
                parent::setAcfFieldValue($acfFieldName, $value);
            }

            public function getCalledFieldName()
            {
                return $this->calledFieldName;
            }

            public function clearCalledFieldName()
            {
                $this->calledFieldName = '';
            }
        };
        $group->load();
        $group->fieldName = 2.0;
        $group->save();

        $this->assertEquals('group_fieldName', $group->getCalledFieldName());

        $group->clearCalledFieldName();
        $group->save();

        $this->assertEmpty($group->getCalledFieldName());
    }

    public function testSaveFieldWhenLoadWasNotCalled(): void
    {
        $group = new class ($this->getCreator()) extends AcfGroup {
            public float $fieldName;
            private string $calledFieldName;
            private $calledFieldValue;

            public function __construct(CreatorInterface $creator)
            {
                parent::__construct($creator);

                $this->calledFieldName = '';
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_fieldName';
            }

            protected function setAcfFieldValue(string $acfFieldName, $value): void
            {
                $this->calledFieldName = $acfFieldName;
                $this->calledFieldValue = $value;
                parent::setAcfFieldValue($acfFieldName, $value);
            }

            public function getCalledFieldName()
            {
                return $this->calledFieldName;
            }

            public function getCalledFieldValue()
            {
                return $this->calledFieldValue;
            }
        };

        $group->fieldName = 2.0;
        $group->save();

        $this->assertEquals('group_fieldName', $group->getCalledFieldName());
        $this->assertTrue(2.0 === $group->getCalledFieldValue());
    }

    public function testSaveCloneField()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {

            public string $fieldName;
            private string $savedFieldName;

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return 'fieldValue';
            }

            protected function setAcfFieldValue(string $acfFieldName, $value): void
            {
                $this->savedFieldName = $acfFieldName;
                parent::setAcfFieldValue($acfFieldName, $value);
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'fieldName';
            }

            public function getSavedFieldName()
            {
                return $this->savedFieldName;
            }
        };
        $fieldInfo = $this->getFieldInfo('sibling', get_class($siblingClass));
        $groupClass = new class ($fieldInfo, $creator) extends AcfGroup {
            public $sibling;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_sibling';
            }
        };

        $groupClass->save();

        $this->assertEquals('group_sibling_fieldName', $groupClass->sibling->getSavedFieldName());
    }

    public function testSaveRepeaterField()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {

            public string $fieldName;

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return 'fieldValue';
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'fieldName';
            }

        };
        $fieldInfo = $this->getFieldInfo('repeater', 'array', [
            'item' => get_class($siblingClass),
        ]);
        $groupClass = new class ($fieldInfo, $creator) extends AcfGroup {
            public $repeater;
            private static FieldInfoInterface $fieldInfo;
            private string $savedFieldName;
            private $savedFieldValue;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
                $this->savedFieldName = '';
                $this->savedFieldValue = '';
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return [[], []];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_repeater';
            }

            protected function setAcfFieldValue(string $acfFieldName, $value): void
            {
                $this->savedFieldName = $acfFieldName;
                $this->savedFieldValue = $value;
                parent::setAcfFieldValue($acfFieldName, $value);
            }

            public function getSavedFieldName()
            {
                return $this->savedFieldName;
            }

            public function getSavedFieldValue()
            {
                return $this->savedFieldValue;
            }
        };

        $groupClass->load();
        $groupClass->repeater[0]->fieldName = 'test';
        $groupClass->save();


        $this->assertEquals('group_repeater', $groupClass->getSavedFieldName());
        $this->assertEquals([
            [
                'fieldName' => 'test',
            ],
            [
                'fieldName' => 'fieldValue',
            ]
        ], $groupClass->getSavedFieldValue());
        $this->assertFalse($groupClass->repeater[0]->isHasChanges());
        $this->assertFalse($groupClass->isHasChanges());
    }

    public function testSaveRepeaterFieldWhenIndexesAreTheSame()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {

            public string $fieldName;
            private string $newClonePrefix;

            public function __construct(CreatorInterface $creator)
            {
                parent::__construct($creator);
                $this->newClonePrefix = '';
            }

            public function setClonePrefix(string $clonePrefix): void
            {
                if ($this->getClonePrefix() !== $clonePrefix) {
                    $this->newClonePrefix = $clonePrefix;
                }
                parent::setClonePrefix($clonePrefix);
            }

            public function getNewClonePrefix(): string
            {
                return $this->newClonePrefix;
            }

        };
        $fieldInfo = $this->getFieldInfo('repeater', 'array', [
            'item' => get_class($siblingClass),
        ]);
        $groupClass = new class ($fieldInfo, $creator) extends AcfGroup {
            public $repeater;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return [[], []];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_repeater';
            }
        };

        $groupClass->load();
        $groupClass->repeater[0]->fieldName = 'test';
        $groupClass->save();

        $this->assertEmpty($groupClass->repeater[0]->getNewClonePrefix());
        $this->assertEmpty($groupClass->repeater[1]->getNewClonePrefix());
    }

    public function testSaveRepeaterFieldWhenIndexesAreTheChanged()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {

            public string $fieldName;
            private string $newClonePrefix;

            public function __construct(CreatorInterface $creator)
            {
                parent::__construct($creator);
                $this->newClonePrefix = '';
            }

            public function setClonePrefix(string $clonePrefix): void
            {
                if ($this->getClonePrefix() !== $clonePrefix) {
                    $this->newClonePrefix = $clonePrefix;
                }
                parent::setClonePrefix($clonePrefix);
            }

            public function getNewClonePrefix(): string
            {
                return $this->newClonePrefix;
            }

        };
        $fieldInfo = $this->getFieldInfo('repeater', 'array', [
            'item' => get_class($siblingClass),
        ]);
        $groupClass = new class ($fieldInfo, $creator) extends AcfGroup {
            public $repeater;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            protected function getAcfFieldValue(FieldInfoInterface $fieldInfo)
            {
                return [[], []];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_repeater';
            }
        };

        $groupClass->load();
        $item = $groupClass->repeater[1];
        $oldClonePrefix = $item->getClonePrefix();
        array_splice($groupClass->repeater, 0, 1);
        $groupClass->save();

        $this->assertCount(1, $groupClass->repeater);
        $this->assertTrue($item === $groupClass->repeater[0]);
        $this->assertEquals('group_repeater_1_', $oldClonePrefix);
        $this->assertEquals('group_repeater_0_', $groupClass->repeater[0]->getNewClonePrefix());
    }

    //

    public function testSaveToArrayAndLoadFromIt()
    {
        $creator = $this->getCreator();
        $group = new class ($creator) extends AcfGroup {
            public string $content;
            public int $price;
        };
        $newGroup = $group->getDeepClone();
        $group->price = 2;
        $group->content = 'some content';
        $data = $group->getFieldValues();

        $newGroup->load(false, '', $data);

        $this->assertEquals(2, $newGroup->price);
        $this->assertEquals('some content', $newGroup->content);
    }

    public function testSaveToArrayAndLoadFromItWhenClone()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            public string $content;
        };
        $fieldInfo = $this->getFieldInfo('sibling', get_class($siblingClass));
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $sibling;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_sibling';
            }
        };
        $newGroup = $group->getDeepClone();
        $group->sibling->content = 'some content';
        $data = $group->getFieldValues();


        $newGroup->load(false, '', $data);


        $this->assertEquals('some content', $newGroup->sibling->content);
    }

    public function testSaveToArrayAndLoadFromItWhenRepeater()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('repeater', 'array', [
            'item' => get_class($siblingClass),
        ]);
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $repeater;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_repeater';
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }
        };
        $newGroup = $group->getDeepClone();
        $group->repeater[0] = $siblingClass->getDeepClone();
        $group->repeater[0]->content = 'some content';
        $data = $group->getFieldValues();


        $newGroup->load(false, '', $data);


        $this->assertCount(1, $newGroup->repeater);
        $this->assertEquals('some content', $newGroup->repeater[0]->content);
    }

    ////

    public function testGetFieldValuesForOrdinaryField()
    {
        $creator = $this->getCreator();
        $group = new class ($creator) extends AcfGroup {
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'content';
            }
        };


        $group->content = 'some content';


        $this->assertEquals([
            'content' => 'some content',
        ], $group->getFieldValues());
    }

    public function testGetFieldValuesForOrdinaryFieldWhenDefaultByTypeIsSaved()
    {
        $creator = $this->getCreator();
        $group = new class ($creator) extends AcfGroup {
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'content';
            }
        };


        $this->assertEquals([
            'content' => '',
        ], $group->getFieldValues());
    }

    public function testGetFieldValuesForOrdinaryFieldWhenDefaultByTypeIsSkipped()
    {
        $creator = $this->getCreator();
        $group = new class ($creator) extends AcfGroup {
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'content';
            }
        };


        $this->assertEmpty($group->getFieldValues('', true));
    }

    public function testGetFieldValuesForOrdinaryFieldWhenDefaultByArgumentIsSaved()
    {
        $creator = $this->getCreator();
        $group = new class ($creator) extends AcfGroup {
            /**
             * @default_value image
             */
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'content';
            }
        };

        $group->content = 'image';

        $this->assertEquals([
            'content' => 'image',
        ], $group->getFieldValues());
    }

    public function testGetFieldValuesForOrdinaryFieldWhenDefaultByArgumentIsSkipped()
    {
        $creator = $this->getCreator();
        $group = new class ($creator) extends AcfGroup {
            /**
             * @default_value image
             */
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'content';
            }
        };

        $group->content = 'image';

        $this->assertEmpty($group->getFieldValues('', true));
    }

    public function testGetFieldValuesForCloneField()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('sibling', get_class($siblingClass));
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $sibling;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_sibling';
            }
        };


        $group->sibling->content = 'some content';


        $this->assertEquals([
            'group_sibling_sibling_content' => 'some content',
        ], $group->getFieldValues());
    }

    public function testGetFieldValuesForCloneFieldWheDefaultByTypeIsSaved()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('sibling', get_class($siblingClass));
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $sibling;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_sibling';
            }
        };


        $this->assertEquals([
            'group_sibling_sibling_content' => '',
        ], $group->getFieldValues());
    }

    public function testGetFieldValuesForCloneFieldWheDefaultByTypeIsSkipped()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('sibling', get_class($siblingClass));
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $sibling;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_sibling';
            }
        };


        $this->assertEmpty($group->getFieldValues('', true));
    }

    public function testGetFieldValuesForCloneFieldWheDefaultByArgumentIsSaved()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            /**
             * @default_value image
             */
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('sibling', get_class($siblingClass));
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $sibling;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_sibling';
            }
        };

        $group->sibling->content = 'image';

        $this->assertEquals([
            'group_sibling_sibling_content' => 'image',
        ], $group->getFieldValues());
    }

    public function testGetFieldValuesForCloneFieldWheDefaultByArgumentIsSkipped()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            /**
             * @default_value image
             */
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('sibling', get_class($siblingClass));
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $sibling;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_sibling';
            }
        };

        $group->sibling->content = 'image';

        $this->assertEmpty($group->getFieldValues('', true));
    }

    public function testGetFieldValuesForRepeaterField()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('repeater', 'array', [
            'item' => get_class($siblingClass),
        ]);
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $repeater;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_repeater';
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }
        };
        $group->repeater[0] = $siblingClass->getDeepClone();
        $group->repeater[0]->content = 'some content';


        $this->assertEquals([
            'group_repeater' => [
                [
                    'sibling_content' => 'some content',
                ],
            ],
        ], $group->getFieldValues());
    }

    public function testGetFieldValuesForRepeaterFieldWhenDefaultByTypeIsSaved()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('repeater', 'array', [
            'item' => get_class($siblingClass),
        ]);
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $repeater;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_repeater';
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }
        };
        $group->repeater[0] = $siblingClass->getDeepClone();


        $this->assertEquals([
            'group_repeater' => [
                [
                    'sibling_content' => '',
                ],
            ],
        ], $group->getFieldValues());
    }

    public function testGetFieldValuesForRepeaterFieldWhenDefaultByTypeIsSkipped()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('repeater', 'array', [
            'item' => get_class($siblingClass),
        ]);
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $repeater;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_repeater';
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }
        };
        $group->repeater[0] = $siblingClass->getDeepClone();

        $this->assertEmpty($group->getFieldValues('', true));
    }

    public function testGetFieldValuesForRepeaterFieldWhenDefaultByArgumentIsSaved()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            /**
             * @default_value image
             */
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('repeater', 'array', [
            'item' => get_class($siblingClass),
        ]);
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $repeater;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_repeater';
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }
        };
        $group->repeater[0] = $siblingClass->getDeepClone();

        $group->repeater[0]->content = 'image';

        $this->assertEquals([
            'group_repeater' => [
                [
                    'sibling_content' => 'image',
                ],
            ],
        ], $group->getFieldValues());
    }

    public function testGetFieldValuesForRepeaterFieldWhenDefaultByArgumentIsSkipped()
    {
        $creator = $this->getCreator();
        $siblingClass = new class ($creator) extends AcfGroup {
            /**
             * @default_value image
             */
            public string $content;

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'sibling_content';
            }
        };
        $fieldInfo = $this->getFieldInfo('repeater', 'array', [
            'item' => get_class($siblingClass),
        ]);
        $group = new class ($fieldInfo, $creator) extends AcfGroup {
            public $repeater;
            private static FieldInfoInterface $fieldInfo;

            public function __construct($fieldInfo, CreatorInterface $creator)
            {
                self::$fieldInfo = $fieldInfo;
                parent::__construct($creator);
            }

            public static function getAcfFieldName(string $fieldName): string
            {
                return 'group_repeater';
            }

            public static function getFieldsInfo(): array
            {
                return [self::$fieldInfo];
            }
        };
        $group->repeater[0] = $siblingClass->getDeepClone();

        $group->repeater[0]->content = 'image';

        $this->assertEmpty($group->getFieldValues('',true));
    }

    ////

    public function testConvertRepeaterFieldValuesFromAcfFormat()
    {
        $data = AcfGroup::convertRepeaterFieldValues('group_repeater', [
            'row-0' => [
                'group_repeater_item_sibling_content' => 'some content',
            ],
        ], true);

        $this->assertEquals([
            [
                'sibling_content' => 'some content',
            ],
        ], $data);
    }

    public function testConvertRepeaterFieldValuesToAcfFormat()
    {
        $data = AcfGroup::convertRepeaterFieldValues('group_repeater', [
            [
                'sibling_content' => 'some content',
            ],
        ], false);

        $this->assertEquals([
            'row-0' => [
                'group_repeater_item_sibling_content' => 'some content',
            ],
        ], $data);
    }

    public function testConvertCloneFieldFromAcfFormat()
    {
        $data = AcfGroup::convertCloneField('local_acf_views_acf-card-data__meta-filter', [
            'local_acf_views_acf-card-data__meta-filter_local_acf_views_meta-filter__rules' => [
                '635930affb610' => [
                    'local_acf_views_meta-filter__rules_item_local_acf_views_meta-rule__fields' => [
                        '635930b0fb611' => [
                            'local_acf_views_meta-rule__fields_item_local_acf_views_meta-field__group' => 'some group',
                        ]
                    ]
                ]
            ],
        ], true);

        $this->assertEquals([
            'local_acf_views_acf-card-data__meta-filter_local_acf_views_meta-filter__rules' => [
                '635930affb610' => [
                    'local_acf_views_meta-rule__fields' => [
                        '635930b0fb611' => [
                            'local_acf_views_meta-field__group' => 'some group',
                        ]
                    ]
                ],
            ],
        ], $data);
    }

    public function testConvertCloneFieldToAcfFormat()
    {
        $data = AcfGroup::convertCloneField('group_repeater', [
            [
                'sibling_content' => 'some content',
            ],
        ], false);

        $this->assertEquals([
            'row-0' => [
                'group_repeater_item_sibling_content' => 'some content',
            ],
        ], $data);
    }
}
