<?php

declare(strict_types=1);

use Codeception\Test\Unit;
use LightSource\AcfGroups\FieldInfo;

class FieldInfoTest extends Unit
{
    private function getReflectionProperty(string $type = '', string $docComment = ''): ReflectionProperty
    {
        return $this->makeEmpty(ReflectionProperty::class, [
            'getName'       => function () {
                return '';
            },
            'getDocComment' => function () use ($docComment) {
                return $docComment;
            },
            'getType'       => function () use ($type) {
                return $this->makeEmpty(ReflectionNamedType::class, [
                    'getName' => function () use ($type) {
                        return $type;
                    }
                ]);
            }
        ]);
    }

    public function testGetTypeForBool()
    {
        $fieldInfo = new FieldInfo($this->getReflectionProperty('bool'));

        $this->assertEquals('bool', $fieldInfo->getType());
    }

    public function testGetTypeForInt()
    {
        $fieldInfo = new FieldInfo($this->getReflectionProperty('int'));

        $this->assertEquals('int', $fieldInfo->getType());
    }

    public function testGetTypeForFloat()
    {
        $fieldInfo = new FieldInfo($this->getReflectionProperty('float'));

        $this->assertEquals('float', $fieldInfo->getType());
    }

    public function testGetTypeForString()
    {
        $fieldInfo = new FieldInfo($this->getReflectionProperty('string'));

        $this->assertEquals('string', $fieldInfo->getType());
    }

    public function testGetTypeForArray()
    {
        $fieldInfo = new FieldInfo($this->getReflectionProperty('array'));

        $this->assertEquals('array', $fieldInfo->getType());
    }

    public function testGetTypeWhenUnsupportedIsIgnored()
    {
        $fieldInfo = new FieldInfo($this->getReflectionProperty('self'));

        $this->assertEmpty($fieldInfo->getType());
    }

    public function testGetArguments()
    {
        $fieldInfo = new FieldInfo(
            $this->getReflectionProperty(
                '',
                '/**
             * @first_argument first-value
             * @second-argument second_value
             * @third-argument \'some value"
             */'
            )
        );

        $this->assertEquals([
                                'a-order'         => 1,
                                'first_argument'  => 'first-value',
                                'second-argument' => 'second_value',
                                'third-argument'  => '\'some value"',
                            ],
                            $fieldInfo->getArguments());
    }

    public function testGetArgumentsWhenValueWithSpaces()
    {
        $fieldInfo = new FieldInfo(
            $this->getReflectionProperty(
                '',
                '/**
             * @instructions For developers only. This field...
             */'
            )
        );

        $this->assertEquals([
                                'a-order'      => 1,
                                'instructions' => 'For developers only. This field...',
                            ],
                            $fieldInfo->getArguments());
    }

    public function testGetArgumentsWhenValueIsJsonObject()
    {
        $fieldInfo = new FieldInfo(
            $this->getReflectionProperty(
                '',
                '/**
             * @choices {"left":"Left","center": "Center","right": "Right"}
             */'
            )
        );

        $this->assertEquals([
                                'a-order' => 1,
                                'choices' => [
                                    'left'   => 'Left',
                                    'center' => 'Center',
                                    'right'  => 'Right',
                                ],
                            ],
                            $fieldInfo->getArguments());
    }

    public function testGetArgumentsWhenValueIsJsonArray()
    {
        $fieldInfo = new FieldInfo(
            $this->getReflectionProperty(
                '',
                '/**
             * @post_types ["first"]
             */'
            )
        );

        $this->assertEquals([
                                'a-order'    => 1,
                                'post_types' => [
                                    'first',
                                ],
                            ],
                            $fieldInfo->getArguments());
    }

    public function testIsRepeaterTrueForArrayWithItem()
    {
        $fieldInfo = new FieldInfo(
            $this->getReflectionProperty(
                'array',
                '/**
             * @item test
             */'
            )
        );

        $this->assertTrue($fieldInfo->isRepeater());
    }

    public function testIsRepeaterFalseForArrayWithoutItem()
    {
        $fieldInfo = new FieldInfo(
            $this->getReflectionProperty(
                'array',
                '/**
             * @some test
             */'
            )
        );

        $this->assertFalse($fieldInfo->isRepeater());
    }
}
